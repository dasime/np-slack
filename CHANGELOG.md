# CHANGELOG

## HEAD

* [FOOBAR2000] Changed the parsed output format from JSON to more simple `Key:Value` pairs.
* More playing with the directory structure.
* Move from `jscs` and `jshint` to `eslint`.

## v0.0.3 / 2017-09-20

* [NEW] Added LASTFM parser.
* Changed the config system from an undocumented `./config.js` to a documented `./config.ini`.
* Break up the code into more logical files. Make `np-slack` more modular by default,
  by moving the commandline interface to `./lib/cli`.

## v0.0.2 / 2016-09-16

* Move `index.js` into `./lib/index.js`. 

## v0.0.1 / 2016-01-11

* Initial Release
