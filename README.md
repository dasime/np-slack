# Now Playing for Slack
Send now playing information to Slack via WebHooks. Currently supports Foobar2000 and LastFM.

## Dependencies ##

 * Now Playing Simple Foobar2000 extension:
 <http://skipyrich.com/wiki/Foobar2000:Now_Playing_Simple> (only for Foobar2000 route).

## Installing ##
Download the program, then run `npm install`.

## Configuring ##
### `config.ini` ###
The is an example configuration in `config_example.ini`, this has all the configuration options
for the program. It's recommended you use this as a template for your own configuration but keep
the example as a reference.

Make sure you need to set a webhook for your Slack in `config.ini`, they can be created here:
<https://my.slack.com/services/new/incoming-webhook/>

By specifying the format you can customise the output to slack. This includes markdown!

    `Now Playing:` *{{title}}* _by_ *{{artist}}* _from_ *{{album}}* _released_ *{{date}}*

You can even add newlines and links like this:

  	`Now Playing` *{{title}}* _by_ *{{artist}}* _from_ *{{album}}* _released_ *{{date}}*\n<https://www.youtube.com/results?search_query=%artist% %title%|YouTube Search>

### Foobar ###
In the `Now Playing Simple` settings in the preferences menu in Foobar2000
set the output to `UTF-8` and define the path.

The default formatting string is:

    $if(%isplaying%,
    $if(%ispaused%,
    paused
    ,
    playing
    ): %artist% - %title%,
    stopped
    )

Which produces something like:

    paused: BiS - Hide out cut

The formatting string must be set to construct `key: value` pairs like below:

    album: %album%$crlf()
    artist: %artist%$crlf()
    date: %date%$crlf()
    title: %title%$crlf()

Which produces something like:

    album: WHO KiLLED IDOL?
    artist: BiS
    date: 2014
    title: Hide out cut

The `$crlf()` at the end of the line is required to insert a newline character.
There must only be one `key: value` pair per line, but whitespace and padding is ignored
(so `key : value`, `key:   value` and `    key    :  value   ` are all fine).

## Usage ##
To run the program, use:

    npm start <profile>

Where `<profile>` is the name of one of the profile  you have added to the `config.ini`.
This is an optional parameter and if you don't specify a profile
the program will attempt to just use the defaults.
