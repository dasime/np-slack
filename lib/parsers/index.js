var foobar = require('./foobar'),
  lastfm = require('./lastfm');

module.exports.foobar = foobar;
module.exports.lastfm = lastfm;
