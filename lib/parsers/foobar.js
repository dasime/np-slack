var fs = require('fs');

module.exports = {
  getNowPlaying: function (options, cb) {
    fs.readFile(options.foobar_path, 'utf8', function (err, data) {
      if (err) {
        return cb('Unable to read the now playing file');
      } else {
        return simpleKeyValueParse(data, cb);
      }
    });
  }
};

/**
 * Takes a multi-line `Key: Value` string as input, splits it
 *     and returns the `{key:value}` pairs as an Object.
 * @param  {string}   input A string with Windows line endings (CRLF)
 * @param  {Function} cb    Standard `function (err, res) {...}` pattern
 * @return {object}         err should contain a string,
 *                              res should contain the YAML-like input as a JS Object `{key: value}`
 */
function simpleKeyValueParse (input, cb) {
  var parsedResponse = {},
    errors = [];
  input
    .split('\r\n') // foobar is Windows-only so always uses Windows-style line endings
    .forEach(function (line) {
      var tokens;
      if (line !== '') { // Ignore empty lines
        tokens = line.split(/:(.+)/);
        // String.split should return `['key', 'value', '']` where '' is always
        // everything unmatched by `(.+)` (which should never be anything).
        tokens = tokens
          .map(function (token) {
            // Trim whitespace from tokens, there could be padding on either side
            return token.trim();
          })
          .filter(function (token) {
            // Drop empty tokens ('' is falsey)
            return Boolean(token);
          });
        if (tokens.length === 2) {
          // String.spit should return `['key', 'value']`
          parsedResponse[tokens[0]] = tokens[1];
        } else {
          errors.push(tokens);
        }
      }
    }
  );
  if (Object.keys(parsedResponse).length === 0) {
    return cb('Unable to parse the now playing file. \n' +
      'This is likely due to a problem with the formatting from foobar.\n' +
      'It looks like: ' + input);
  }
  if (errors.length > 0) {
    console.warn('There were', errors.length, 'errors while parsing the now playing file');
    console.warn('Unparsable keys:', errors);
  }
  return cb(null, parsedResponse);
}
