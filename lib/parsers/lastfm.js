var request = require('request');

// http://www.last.fm/api/show/user.getRecentTracks

module.exports = {
  getNowPlaying: function (options, cb) {
    request.get({
      url: 'http://ws.audioscrobbler.com/2.0/?method=user.getrecenttracks' +
            '&user=' + options.lastfm_username +
            '&api_key=' + options.lastfm_api_key +
            '&format=json&limit=1'
    }, function (error, response, body) {
      var track,
        tracks;
      if (error) {
        return cb(error);
      } else if (response.statusCode === 200) {
        try {
          tracks = JSON.parse(body);
          if (tracks.recenttracks && tracks.recenttracks.track) {
            // sometimes track is an array of tracks, if it is, we only want the first object
            if (Array.isArray(tracks.recenttracks.track)) {
              track = tracks.recenttracks.track[0];
            } else {
              track = tracks.recenttracks.track;
            }
            return cb(null, {
              'album': track.album['#text'],
              'artist': track.artist['#text'],
              'date': '???',
              'title': track.name
            });
          } else {
            return cb('error parsing lastfm response');
          }
        } catch (e) {
          return cb('error parsing lastfm response as json');
        }
      } else {
        return cb('Unhandled response type: ' + response.statusCode);
      }
    }
    );

  }
};
