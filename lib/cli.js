/* eslint-disable no-undefined */
var _ = require('underscore'),
  commander = require('commander'),
  fs = require('fs'),
  ini = require('ini'),

  npCore = require('./index'),

  npCoreInputDefaults = {
    format: undefined,
    foobar_path: undefined,
    lastfm_username: undefined,
    lastfm_api_key: undefined,
    webhook_url: undefined,
    bot_name: undefined,
    bot_icon: undefined,
    channel: undefined
  };

commander
  .version('', process.env.NPM_PACKAGE_VERSION)
  .usage('[options] <team>')
  .option('-q, --quiet', 'Don\'t send message to Slack')
  // TODO option to specify a .ini file
  .parse(process.argv);

validateProfile(commander.args[0], function (response) {
  console.log(response);
});

function parseConfig (cb) {
  var configFile = './config.ini';
  // TODO Set some default locations to try, rather than hardcoding just one.
  fs.readFile(configFile, 'utf8', function (err, iniRaw) {
    if (err) {
      return cb('Error reading the configuration file.' + err);
    } else {
      return cb(null, ini.parse(iniRaw));
    }
  });
}


// https://www.npmjs.com/package/ini
// https://en.wikipedia.org/wiki/INI_file

function validateProfile (profile, cb) {
  parseConfig(function (err, config) {
    var npCoreInput;
    if (err) {
      return cb(err);
    } else {
      // populate the core input
      npCoreInput = _.defaults(npCoreInputDefaults, _.pick(config, function (value) {
        return !_.isObject(value);
      }));
      // check if there was a profile provided
      if (profile !== undefined) {
        // check if the provided profile exists in the ini file
        if (_.has(config, profile)) {
          npCoreInput = _.extend(npCoreInput, config[profile]);
        } else {
          return cb('Unable to find ' + profile + ' profile.');
        }
      }
      validateCoreObject(npCoreInput, function (error, validObject) {
        if (error) {
          return cb(error);
        } else {
          npCore.retrieveDataAndSend(validObject, function (err, res) {
            return cb(err || res);
          });
        }
      });
    }
  });
}

function validateCoreObject (npCoreInput, cb) {
  var undefinedKeys;
  if (npCoreInput.foobar_path && npCoreInput.lastfm_username && npCoreInput.lastfm_api_key) {
    return cb('Multiple parsers defined, please only specify one parser per profile');
  } else if (npCoreInput.lastfm_username && npCoreInput.lastfm_api_key) {
    delete npCoreInput.foobar_path;
    npCoreInput.parser = 'lastfm';
  } else if (npCoreInput.foobar_path) {
    delete npCoreInput.lastfm_username;
    delete npCoreInput.lastfm_api_key;
    npCoreInput.parser = 'foobar';
  } else {
    return cb('No detected parsers specified, please specify one parser per profile');
  }

  undefinedKeys = _.pick(npCoreInput, function (value) {
    return _.isUndefined(value);
  });
  if (Object.keys(undefinedKeys).length > 0) {
    return cb('Undefined required options: ' + _.keys(undefinedKeys).join(', '));
  } else {
    return cb(null, npCoreInput);
  }
}
