var request = require('request'),
  parsers = require('./parsers'),
  renderer = require('./renderer');

module.exports.retrieveDataAndSend = function (options, cb) {
  parsers[options.parser].getNowPlaying(options, function (err, data) {
    if (err) {
      return cb(new Error(err));
    } else {
      renderer.render(data, options.format, function (err, rendered) {
        var payload = {
          // unescape newline character, JSON.stringify will escape it.
          text: rendered.replace('\\n', '\n'),
          username: options.bot_name,
          icon_emoji: options.bot_icon
        };
        if (err) {
          return cb('Unable to render response. This should never be called.');
        }
        if (options.channel) {
          payload.channel = options.channel;
        }
        request.post({
          url: options.webhook_url,
          form: {payload: JSON.stringify(payload)}
        }, function (error, response, body) {
          if (error) {
            return cb(error);
          } else if (response.statusCode === 200) {
            return cb(null, body);
          } else {
            return cb('Unhandled response type: ' + response.statusCode);
          }
        }
        );
      });
    }
  });
};
