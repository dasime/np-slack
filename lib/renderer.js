module.exports.render = function (npObject, prototype, cb) {
  var tokens = prototype.match(/{{\w*}}/g);
  if (typeof npObject === 'object') {
    if (typeof tokens === 'object' && tokens.length > 0) {
      tokens.forEach(function (item) {
        prototype = prototype.replace(item, npObject[item.match(/{{(\w*)}}/)[1]] || '???');
      });
      return cb(null, prototype);
    } else {
      return cb(null, npObject);
    }
  } else {
    return cb(null, npObject);
  }
};
